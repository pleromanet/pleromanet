(ns pleromanet.sephirot)

(defn crown []
  [:h2 "Crown"]
  )

(defn understanding []
  [:h2 "Understanding"]
  )

(defn wisdom []
  [:h2 "Wisdom"]
  )

(defn knowledge []
  [:h2 "Knowledge"]
  )

(defn strength []
  [:h2 "Strength"]
  )

(defn beauty []
  [:h2 "Beauty"]
  )

(defn splendor []
  [:h2 "Splendor"]
  )

(defn victory []
  [:h2 "Victory"]
  )

(defn foundation []
  [:h2 "Foundation"]
  )

(defn kingdom []
  [:h2 "Kingdom"]
  )
